{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Subprocess Strategy"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Running scripts"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Running scripts from the command line via a terminal environment is similar both in R and Python.\n",
    "The command to be run contains the following parts,\n",
    "\n",
    "    <command> <path_to_script> <additional_arguments>\n",
    "    \n",
    "where\n",
    "\n",
    "* `<command>` is the executable to run (`Rscript` for R and `python3` for Python code)\n",
    "* `<path_to_script>` is the full or relative path to the script being executed.\n",
    "* `<additional_arguments>` contains a space separated list of arguments parsed to the script itself."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "So for example, an R script is executed by running the following command:\n",
    "\n",
    "    Rscript path/to/myscript.R arg1 arg2 arg3"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Accessing command line Arguments"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### R"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The example above receives arguments `arg1`, `arg2`, and `arg3`.\n",
    "These are accessible using the `commandArgs` function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```r\n",
    "## myscript.R\n",
    "\n",
    "# Fetch command line arguments\n",
    "myArgs <- commandArgs(trailingOnly = TRUE)\n",
    "\n",
    "# myArgs is a character vector of all arguments\n",
    "print(myArgs)\n",
    "print(class(myArgs))\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "By setting `trailingOnly = TRUE`, the vector `myArgs` only contains arguments that you added on the command line.\n",
    "If left `FALSE` (default), there will be other arguments included in the vector, such as the path to the script that is executed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Arguments in Python scripts can be accessed by importing the `sys` module.\n",
    "The module holds parameters and functions that are system specific.\n",
    "Its `argv` attribute contains a list of all the arguments passed to the script.\n",
    "The first element is always the full file path to the script being executed."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "```python\n",
    "## myscript.py\n",
    "import sys\n",
    "\n",
    "# Fetch command line arguments\n",
    "my_args = sys.argv\n",
    "\n",
    "# my_args is a list, containing the scripts path at position 0\n",
    "print(my_args)\n",
    "print(type(my_args))\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "As with the R example above, all arguments are parsed as strings, so you need to consider this.\n",
    "However, there is also the module [`argparse`](https://docs.python.org/3/library/argparse.html) that offers parsing of command-line arguments (besides many more features)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Executing a subprocess"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Python"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To execute an R script in Python we make use of the [`subprocess`](https://docs.python.org/3/library/subprocess.html) module.\n",
    "To run a command and return its output, the method `subprocess.check_output()` can be used."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import subprocess\n",
    "print(subprocess.check_output(\"ls\", universal_newlines=True))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "To execute an R script that takes several arguments, you first have to build the command to be executed.\n",
    "In Python this is represented as a list of strings, whose elements correspond to the following:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "[\"<command>\", \"<path_to_script>\", \"arg1\", \"arg2\", \"arg3\"]\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "An example of executing a simple R script is given in the following code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import subprocess\n",
    "\n",
    "# define command and arguments\n",
    "command = \"Rscript\"\n",
    "script_path = \"/tmp/maximum.R\"\n",
    "\n",
    "# write a small R script for testing\n",
    "with open(script_path, \"w+\") as max_r_script:\n",
    "    # save arguments to myArgs\n",
    "    max_r_script.write(\"myArgs <- commandArgs(trailingOnly = TRUE)\\n\")\n",
    "    # convert arguments to numeric\n",
    "    max_r_script.write(\"values <- as.numeric(myArgs)\\n\")\n",
    "    # print maximum value to stdout\n",
    "    max_r_script.write(\"cat(max(values))\\n\")\n",
    "\n",
    "# number of args in a list\n",
    "args = [\"11\", \"3\", \"9\", \"42\"]\n",
    "\n",
    "# build subprocess command\n",
    "cmd = [command, script_path] + args\n",
    "\n",
    "x = subprocess.check_output(cmd, universal_newlines=True)\n",
    "print(\"The maximum of the numbers is: %s\" %x)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## R"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "When executing subprocesses with R, it is recommended to use R's `system2` function to execute and capture the output.\n",
    "This is due to the `system` function not being cross-platform compatible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Building up the command to be executed is similar to the provided Python example.\n",
    "An example of executing a script from R is given in the following code."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```r\n",
    "command <- \"python\"\n",
    "\n",
    "scriptPath <- \"/path/to/script.py\"\n",
    "\n",
    "# build up args in a vector\n",
    "argumentOne <- \"hello\"\n",
    "argumentTwo <- \"world\"\n",
    "args <- c(argumentOne, argumentTwo)\n",
    "\n",
    "# Add path to script as first arg\n",
    "allArgs <- c(scriptPath, args)\n",
    "\n",
    "output <- system2(command, args=allArgs, stdout=TRUE)\n",
    "print(paste(\"Received result from python:\\n\" output))\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<div class=\"alert alert-info\">\n",
    "Python has the command line option `-c` that allows to specify a command to execute. \n",
    "Following options are passed as arguments to the command.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Summary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "It is possible to integrate Python and R into a single application via the use of subprocess calls. \n",
    "These allow one parent process to call another as a child process and capture any output that is printed to stdout."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.0"
  },
  "livereveal": {
   "scroll": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
