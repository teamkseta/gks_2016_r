{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# R in a Cage: PypeR"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[PyperR](http://www.webarray.org/softwares/PypeR/) provides a simple way to access R from Python through pipes.\n",
    "It provides a class `R` that wraps the R language.\n",
    "An instance of the `R` class is used to manage an R process.\n",
    "Different instances can use different R installations.\n",
    "It is even possible to use an R installed on a remote machine.\n",
    "\n",
    "This enables data scientists to do the data munging with Python and the statistical analysis with R by passing objects interactively between the two languages."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "PypeR requires Python 2.3 or later."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Basic Usage"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To use PypeR, the first step is to import it into Python."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "import pyper as pr"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "To execute something in R you first need to initialise a new R session."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "r = pr.R()\n",
    "print(r(\"a <- 3; print(a + 5)\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "By default, PypeR starts R sessions in Debug mode. \n",
    "Therefore each call is encapsulated into a `try` function.\n",
    "This prevents R from crashing.\n",
    "\n",
    "You can disable the debug mode by setting the variable `_DEBUG_MODE` in the R class or in its instance to `False`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "r._DEBUG_MODE = False\n",
    "print(r(\"a <- 3; print(a + 5)\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Available Methods"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "An instance of the `R` class offers four different methods:\n",
    "\n",
    "* `run`,\n",
    "* `assign`,\n",
    "* `get`, and\n",
    "* `remove`"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### The `run` Method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This method is used to pass a command string to R.\n",
    "The return value itself is a String, including also the executed commands.\n",
    "If you want the value instead, use the `get` method instead."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# You can execute complex (and simple) expressions\n",
    "r.run(\"1 + 2\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# You can also access defined variables\n",
    "r.run(\"a\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# ... and also define new ones\n",
    "r.run(\"b <- 3\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### The `assign` Method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To better assign a variable (more readable syntax and also looks more pythonic ;)) you might use the `assign` method.\n",
    "The method itself has no return value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Assign a value to variable\n",
    "r.assign(\"b\", 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### The `get` Method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To get a result of an R expression (or the value of a variable) you can use the `get` method.\n",
    "Here, you only get its value."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Getting the value of a variable\n",
    "r.get(\"b\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# Getting a default if a variable is not defined\n",
    "print(r.get(\"c\", None))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# This also works for expressions\n",
    "r.get(\"1 + 1\", 3)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### The `remove` Method"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "To remove a variable you defined you can use the `remove` Method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Remove variable c\n",
    "r.remove(\"b\")\n",
    "print(r.get(\"b\"))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Calling the instance"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The instance of the `R` is callable and can be called as a function.\n",
    "This equals calling the `run` method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Accessing variable a\n",
    "r(\"a\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Evaluating expression\n",
    "r(\"1 + 2\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Using the instance as Python Dictionary"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The instance also mimics some operations of Python dictionaries.\n",
    "E.g. you can assign values to R variables or retrieve values for any R expression.\n",
    "You can also delete R variables."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "# Setting value of a variable\n",
    "r[\"a\"] = 4"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# Getting value of a variable\n",
    "r[\"a\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Getting value of expression\n",
    "r[\"1 + 2\"]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "r[\"c\"] = 1\n",
    "# Deleting variable\n",
    "del r[\"c\"]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Accessing Variables like Attributes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Finally, you can also access R variables as if they were attributes of the instance.\n",
    "If the variable name cannot be found in the instance or its class, the instance will try to get/set/remove it in R.\n",
    "\n",
    "<div class=\"alert alert-warning\">\n",
    "Please take care not to use variable names containing a dot.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Accessing the value of variable\n",
    "print(r.a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# Setting a value\n",
    "r.a = 5\n",
    "print(r.a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Working with (more) Complex Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "collapsed": true
   },
   "source": [
    "You can easily create e.g. a DataFrame using `pandas`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "import pandas as pd\n",
    "\n",
    "df = pd.read_csv(file_path)\n",
    "```\n",
    "\n",
    "Such a DataFrame can directly be assigned to the R Environment.\n",
    "\n",
    "```python\n",
    "r.assign(\"rdata\", df)\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Attention"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Incomplete Strings"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There are some issues that can be fatal when using PypeR.\n",
    "Incomplete strings for example can lead to a dead waiting: R is waiting for more input from Python while Python is trying to get output from R."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.5.0"
  },
  "livereveal": {
   "scroll": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
