{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Introduction"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "This is a basic introduction into a subset of R's basic functionality.\n",
    "The main goal is to get a feeling for R.\n",
    "\n",
    "Everything that R creates is an object.\n",
    "Each of those can have various attributes such as `class`, `length`, `names`, dimensions, and metadata.\n",
    "The attributes can be obtained with the `attributes` function."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Basic Data Classes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Following, we look at some ways to store and organise data with R.\n",
    "The most basic way to store data is to make assignments.\n",
    "\n",
    "There are three basic classes of data objects:\n",
    "\n",
    "* numeric,\n",
    "* character, and\n",
    "* logical."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# numbers\n",
    "a <- 3\n",
    "# character strings\n",
    "b <- \"hello\"\n",
    "# logical\n",
    "c <- (5 + 4 == 9)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The `<-` operator tells R to store data into the variable given on the left.\n",
    "The value of a variable can be printed by just entering the name of the variable.\n",
    "\n",
    "Missing data is indicated by `NA` (not available).\n",
    "\n",
    "The logical type has two predefined values, `TRUE` and `FALSE`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true
   },
   "outputs": [],
   "source": [
    "a <- c(TRUE, FALSE)\n",
    "b <- c(FALSE, FALSE)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "<div class=\"alert alert-warning\">\n",
    "Attention: there is a difference between operators that act on entries within a vector, and the vector itself.\n",
    "</div>"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a | b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a || b"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "xor(a, b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "The standard logical operators can be used\n",
    "\n",
    "| Operator    | Description           |\n",
    "|:------------|-----------------------|\n",
    "| `<`         | less than             |\n",
    "| `>`         | greater than          |\n",
    "| `<=`        | less than or equal    |\n",
    "| `>=`        | greater than or equal |\n",
    "| `==`        | equal to              |\n",
    "| `!=`        | not equal to          |\n",
    "| &#124;      | entry-wise or         |\n",
    "| &#124;&#124;| or                    |\n",
    "| `!`         | not                   |\n",
    "| `&`         | entry-wise and        |\n",
    "| `&&`        | and                   |\n",
    "| `xor(a, b)` | exclusive or          |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Data Objects"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Data objects include vectors, matrices, lists, and dataframes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Vectors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Vectors are one dimensional ordered collections of data of the same class.\n",
    "\n",
    "You can create vectors by *concatenating* values with the `c` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# vector of numbers\n",
    "x0 <- c(1, 2, 3, 4, 5)\n",
    "# you can create sequences on different ways\n",
    "x1 <- seq(1, 5, by=1)\n",
    "x2 <- 1:5\n",
    "# initialise empty vector of length 5\n",
    "c <- numeric(5)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Data can be accessed by using its index.\n",
    "Indexing is done by using `[]` or `[[]]` for numerical indexes, while `$` is used for named indexes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# access first element\n",
    "x0[1]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Note that the zero entry is used to indicate how the data is stored.\n",
    "The datatype can be determined using the `typeof` command."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# data type of vector\n",
    "typeof(x0)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Matrices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Matrices are two dimensional arrays of the same class."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "m <- matrix(1:6, nrow=2, ncol=3)\n",
    "m"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "attributes(m)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Lists"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Lists are vectors that allow to mix different classes.\n",
    "They are most useful if created with name-value pairs to enable access by name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "person <- list(firstName=\"Max\", lastName=\"Mustermann\", age=20)\n",
    "person"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Dataframes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dataframes are two dimensional tables where the values in each column must be of the same type, but values in different columns may be different.\n",
    "Data read from external files will be put into a dataframe.\n",
    "\n",
    "One example way to create dataframes is the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "a <- seq(1, 4)\n",
    "b <- seq(2, 8, by=2)\n",
    "levels <- factor(rep(c(\"A\", \"B\"), 2))\n",
    "df <- data.frame(first=a, second=b, third=levels)\n",
    "df"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "summary(df)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# accessing single columns\n",
    "df$first"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "df$second"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "df$third"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# access also by index\n",
    "df[[1]]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Subsetting Dataframes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Dataframes can be reduced to a subset of columns using subscripts.\n",
    "To select certain columns for all rows use the following:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "subsetdf <- df[1:2]\n",
    "subsetdf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "#### Filtering Dataframes"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Filters can be used to obtain only those rows that meet certain criteria."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "filtereddf <- df[df$first > 2,]\n",
    "filtereddf"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Factors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "A set of factors have a discrete set of possible values, called *levels* which work similar to enum values.\n",
    "One thing that is important is the number of times that each factor appears, their frequencies."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# R treats values as numeric\n",
    "a <- c(1, 2, 2, 2, 2, 3, 1, 1, 1, 1, 4, 4, 4, 1)\n",
    "# statistics on numeric values\n",
    "summary(a)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# conversion to factor\n",
    "b <- factor(a)\n",
    "# frequencies per level\n",
    "summary(b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# available levels\n",
    "levels(b)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Basic Operations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Most of basic operations work on a whole vector and can be used to perform large number of calculations quickly with a single command.\n",
    "Operations on vectors are performed on an element by element basis."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "a <- 1:4\n",
    "a + 2"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "b <- 10:6\n",
    "# attention when lengths of vectors differ\n",
    "a + b"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "There are also operations that work on lists, e.g. `sort`, `min`, `max`, or `sum`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "min(a, b)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "sum(a)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Control Flow"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# if .. else syntax\n",
    "i <- 4\n",
    "if (i < 2) {\n",
    "    print(i)\n",
    "} else {\n",
    "    print(i * i)\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "Following is the syntax for for loops.\n",
    "There is also while loops (see below) and repeat."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# for loop syntax\n",
    "for (a in 1:3) {\n",
    "    print(a * a)\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "# while loop syntax\n",
    "i <- 1\n",
    "while (i < 4) {\n",
    "    print(i * 2)\n",
    "    i <- i + 1\n",
    "}"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Functions"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Functions are another kind of object in R. \n",
    "You can get a list of builtin functions via `builtins`.\n",
    "Since there are many functions, it is good practice to ensure function and variable names are not already being used.\n",
    "The easiest way to check this is to type the name of proposed function or name.\n",
    "Either you will get an error message or a definition of the function."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "squareMin <- function(a, b) {\n",
    "    x <- min(a, b)\n",
    "    x * x\n",
    "}\n",
    "squareMin(3, 6)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Reading data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "With R you can easily import (and export) various file formats into dataframes.\n",
    "By default, missing data is output as `NA`."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### CSV Data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Sample data can be stored in CSV format.\n",
    "The data can be read with `read.csv`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# read csv into R\n",
    "mydata <- read.csv(\"mydata.csv\", header=TRUE, sep=\",\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "### Excel File"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "If sample data is in Exel format, it can be imported with `read.xls` from *gdata* package."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "library(gdata)\n",
    "# read data from first sheet\n",
    "mydata <- read.xls(\"mydata.xls\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Useful Commands"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Following are some useful commands, that might get handy."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false
   },
   "outputs": [],
   "source": [
    "# terminating an R Session\n",
    "q()\n",
    "# installing new packages\n",
    "install.packages(\"ggplot2\")\n",
    "# loading packages\n",
    "library(ggplot2)\n",
    "# detaching packages\n",
    "detach(\"package:ggplot2\", unload=TRUE)\n",
    "# getting help\n",
    "?ls\n",
    "help(ls)\n",
    "# example section from help\n",
    "example(ls)\n",
    "# argument list of function\n",
    "args(ls)\n",
    "# determine data type of variable\n",
    "typeof(a)\n",
    "# summary about objects\n",
    "summary(a)\n",
    "# structure of arbitrary object\n",
    "str(df)\n",
    "# first or last 6 elements, respectively\n",
    "head(df)\n",
    "tail(df)\n",
    "# list defined variables in session\n",
    "ls()\n",
    "# concatenate and print strings/variables\n",
    "cat(\"Test\", \"String\")\n",
    "# concatenate strings\n",
    "paste(\"Test\", \"String\")\n",
    "# remove a variable from session\n",
    "rm(df)\n",
    "# remove all variables from session\n",
    "rm(list=ls())"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "collapsed": false,
    "slideshow": {
     "slide_type": "skip"
    }
   },
   "outputs": [],
   "source": [
    "sessionInfo()"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "R",
   "language": "R",
   "name": "ir"
  },
  "language_info": {
   "codemirror_mode": "r",
   "file_extension": ".r",
   "mimetype": "text/x-r-source",
   "name": "R",
   "pygments_lexer": "r",
   "version": "3.3.1"
  },
  "livereveal": {
   "scroll": true
  }
 },
 "nbformat": 4,
 "nbformat_minor": 1
}
